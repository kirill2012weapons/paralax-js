/**
 *
document.addEventListener("DOMContentLoaded", function () {

    var paralaxElem = new Parallax({
        // Selector of the Parallax Element
        elementSelector: '[data-parallax-this]',
        // @prop:
        // false -> Set center from the parent
        // 0 -> from bottom to top of the parent
        // Can get value like -1 (slowly) or 5 (faster)
        speed: 5,
        // Direction
        // @prop:
        // reverse, normal
        direction: 'normal'

    });

});
 */

function Parallax(param) {

    var doc = document;

    var _params = param;

    /**
     * @description Private case of fields
     * @type {object}
     * @private
     */
    var _ = {};

    // Speed of the Parallax element
    _.speed = _params['speed'];

    // Selector of the Parallax element
    _.elementSelector = _params['elementSelector'] ? _params['elementSelector'] : false;

    // Options of the Element
    _.element = doc.querySelector(_.elementSelector);
    _.elementHeight = (_.element).clientHeight;

    // Options of the Parent
    _.parentElement = (_.element).offsetParent;
    _.parentElementHeight = (_.parentElement).clientHeight;

    // Directions
    _.direction = param['direction'];

    // Global length
    // From translate Y length
    _.lengthToTopEllement = null;

    /**
     * @param objToCheck
     * @description Checks all keys in object _
     * @return {boolean}
     */


    this.check = function (objToCheck) {

        for (var key in objToCheck) {
            if (key === 'speed') continue;
            if ((objToCheck[key] === 'undefined')   ||
                (objToCheck[key] === 'NaN')         ||
                (objToCheck[key] === 'Null')        ||
                (objToCheck[key] === false)) {
                console.dir('Bad argument: ' + key);
                return true;
            }
        }

    }(_);

    // ON   READY INITIALISATION
    _setPositionElement();
    // END  ON READY INITIALISATION

    /**
     * @description Get height of the window
     * @return {Number}
     * @private
     */
    function _getScreenHeight() {

        return window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;

    }

    /**
     * @return {int}
     * @private
     */
    function _getYTranslate() {

        return _.lengthToTopEllement;

    }

    /**
     *
     * @return {number}
     * @private
     */
    function _getParentHeight() {

        return _.parentElementHeight;

    }

    /**
     *
     * @return {number}
     * @private
     */
    function _getElementHeight() {

        return _.elementHeight;

    }

    /**
     * @description Is parent element visible on the screen
     * @return {boolean}
     * @private
     */
    function _isParentVisible() {

        var par                 = _.parentElement,
            heightParent        = (_.parentElement).offsetHeight,
            topToTopParent      = par.getBoundingClientRect().top,
            bottToTopParent     = topToTopParent + heightParent,
            windowHeight        = _getScreenHeight();

        if (    ((topToTopParent < 0) && (bottToTopParent < 0)) ||
                ((topToTopParent > windowHeight) && (bottToTopParent > windowHeight))   ) return false;
        else return true;

    }

    /**
     * @description Set translate on the element by Y chord
     * @param int
     * @return {boolean}
     * @private
     */
     function _setTranslate3d(int) {

        var _firstPart = 'transform: translate3d(0, ',
            _secondPart = 'px, 0);';

        (_.element).setAttribute('style', _firstPart + Math.round(int) + _secondPart);
        _.lengthToTopEllement = Math.round(int);

        return true;

    }

    /**
     * @description Is element above
     * @return {boolean}
     * @private
     */
    function _isElementAbove() {

        if (!_isParentVisible()) {

            if (window.scrollY > _offsetParentTopFromBottom(_)) return true;

        }

        return false;

    }

    /**
     * @description Is speed false
     * @return {boolean}
     * @private
     */
    function _isSpeedFalse() {

        if (_.speed === false) return true;
        else return false;

    }

    /**
     * @description Is element below
     * @return {boolean}
     * @private
     */
    function _isElementBelow() {

        if (!_isParentVisible()) {

            if (window.scrollY < _offsetParentTopFromTop(_.parentElement)) return true;

        }

        return false;

    }

    /**
     * @description Length -> From top of the current element to the top of page;
     * @return {number}
     * @private
     */
    function _offsetElementTopFromTop() {

        var _buffLength = 0,
            _buffElem = _.element;

        _buffLength += _.lengthToTopEllement;

        while (_buffElem.nodeName !== 'BODY') {

            _buffLength += _buffElem.offsetTop;
            _buffElem = _buffElem.offsetParent;

        }

        return _buffLength;

    }

    /**
     * @description Length ->  From bottom of the current element to the top of page;
     * @return {number}
     * @private
     */
    function _offsetElementTopFromBottom() {

        var _buffLength = 0,
            _buffElem = _.element;

        _buffLength += _.lengthToTopEllement + _buffElem.offsetHeight;

        while (_buffElem.nodeName !== 'BODY') {

            _buffLength += _buffElem.offsetTop;
            _buffElem = _buffElem.offsetParent;

        }

        return _buffLength;

    }

    /**
     * @description Length -> From top of the Parent to the top of page;
     * @return {number}
     * @private
     */
    function _offsetParentTopFromTop() {

        var _buffLength = 0,
            _buffElem = _.parentElement;

        while (_buffElem.nodeName !== 'BODY') {

            _buffLength += _buffElem.offsetTop;
            _buffElem = _buffElem.offsetParent;

        }

        return _buffLength;

    }

    /**
     * @description Length ->  From bottom of the Parent to the top of page;
     * @return {number}
     * @private
     */
    function _offsetParentTopFromBottom() {

        var _buffLength = 0,
            _buffElem = _.parentElement;

        while (_buffElem.nodeName !== 'BODY') {

            _buffLength += _buffElem.offsetTop;
            _buffElem = _buffElem.offsetParent;

        }

        return _buffLength;

    }

    /**
     * @description Set the element from current position of the scroll
     * @return {boolean}
     * @private
     */
    function _setPositionElement() {

        var parent      = _.parentElement,
            element     = _.element;

        if (_.speed === false) {
            _setCenter();
            return true;
        }

        if (_.speed === 0) {

            var point = 0;

            if (_isElementBelow()) {

                point = 0;
                _.lengthToTopEllement = point;
                _setTranslate3d(point);
                return true;

            } else if (_isElementAbove()) {

                point = _getParentHeight() - _getElementHeight();
                _setTranslate3d(point);
                return true;

            }

        }

        if (_.speed !== 0) {

            if (_isElementBelow()) {

                point = 0;
                _.lengthToTopEllement = point;
                _setTranslate3d(point);
                return true;

            } else if (_isElementAbove()) {

                point = _getParentHeight() - _getElementHeight();
                _setTranslate3d(point);
                return true;

            }

            _setTranslate3d(_calculateByFormula());
            return true;

        }

        console.dir('Something wrong...');
        return false;

    }

    /**
     *
     * @return {number}
     * @private
     */
    function _calculateByFormula() {

        var positionY       = 0,
            speed           = _.speed,
            parent          = _.parentElement,
            maxOnScreen     = _getScreenHeight() - _getParentHeight(),
            maxOnParent     = _getParentHeight() - _getElementHeight(),
            direction       = _.direction;

        var _buff = 0;

        switch (direction) {

            case 'reverse':

                if (speed === 0) {

                    positionY = maxOnParent - (((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen));

                } else if (speed < 0) {

                    _buff = -(speed*0.5);
                    positionY = maxOnParent - (((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen)/_buff +
                        (maxOnParent/2 - maxOnParent/2/_buff));

                } else if (speed > 0) {

                    _buff = (speed*0.5);
                    positionY = maxOnParent - (((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen)*_buff +
                        (maxOnParent/2 - maxOnParent/2*_buff));

                }

                break;

            case 'normal':

                if (speed === 0) {

                    positionY = ((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen);

                } else if (speed < 0) {

                    _buff = -(speed*0.5);
                    positionY = ((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen)/_buff +
                        (maxOnParent/2 - maxOnParent/2/_buff);

                } else if (speed > 0) {

                    _buff = (speed*0.5);
                    positionY = ((parent.getBoundingClientRect().top*maxOnParent)/maxOnScreen)*_buff +
                        (maxOnParent/2 - maxOnParent/2*_buff);

                }

                break;

        }

        return positionY;

    }

    /**
     * @description this -> Parallax, Centered the element from parent
     * @return {boolean}
     * @private
     */
    function _setCenter() {

        var _centeredPoint = Math.round((_getParentHeight() / 2) - (_getElementHeight() / 2));
        _setTranslate3d(_centeredPoint);

        return true;

    }

    doc.addEventListener('scroll', function (event) {

        if (_isParentVisible()) {

            if (!_isSpeedFalse()) _setTranslate3d(_calculateByFormula());
            else _setCenter();

        }

    });


}