# Parallax JS

## Native

### Initialisation

    var paralaxElem = new Parallax({
        // Selector of the Parallax Element
        elementSelector: '[data-parallax-this]',
        // @prop:
        // false -> Set center from the parent
        // 0 -> from bottom to top of the parent
        // Can get value like -1 (slowly) or 5 (faster)
        speed: 5,
        // Direction
        // @prop:
        // reverse, normal
        direction: 'normal'

    });
    
### Options
	elementSelector -> Selector
	
	speed -> int|false
	
	direction -> "normal"|"reverse"